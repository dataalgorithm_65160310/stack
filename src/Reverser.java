public class Reverser {
    private String input;
    private String output;

    Reverser(String in) {
        input = in;
    }

    public String doRev() {
        int stackSize = input.length();
        StackY stackArray = new StackY(stackSize);

        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            stackArray.push(ch);
        }
        output = "";
        for (int i = 0; i < input.length(); i++) {
            char ch = stackArray.pop();
            output += ch;
        }
        return output;
    }
}
